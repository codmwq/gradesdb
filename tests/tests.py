import os, sys, inspect
import unittest
from PyQt5.QtSql import QSqlQuery,  QSqlDatabase
from math import floor
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
src_path = os.path.join(parentdir, 'src')
sys.path.insert(0, parentdir)

from src.DataBase import DB


def init_test_table():
    db_connect = QSqlDatabase.addDatabase('QSQLITE')
    db_connect.setDatabaseName('test.db')
    if not db_connect.open():
        print("trouble")
        return False
    query = QSqlQuery()
    query.exec("CREATE TABLE test_grades AS SELECT id, groups, name, avg FROM grades")
    query.exec("CREATE TABLE test_achievements AS SELECT id, achievement, src FROM achievements")
    query.exec("CREATE TABLE test_works AS SELECT id, groups, name, Practical_1,"
               " Deadline_1 FROM works")
    query.exec("CREATE TABLE test_progress AS SELECT id, groups, name, ach1 FROM progress")


def remove_test_table():
    query = QSqlQuery()
    query.exec("DROP TABLE test_grades")
    query.exec("DROP TABLE test_achievements")
    query.exec("DROP TABLE test_works")
    query.exec("DROP TABLE test_progress")

def avg():
    counts_counter = 5
    query = QSqlQuery()
    sql = "WITH tc AS ( SELECT ( "
    for i in range(3, counts_counter, 2):
        sql = sql + 'IFNULL(Practical_' + str(floor(i / 2)) + ', 0) + IFNULL(Deadline_' + str(floor(i / 2)) + ', 0)'
        if (i + 2) < counts_counter:
            sql = sql + ' + '
    sql = sql + ") AS total_count, id FROM test_works) "
    sql = sql + "UPDATE test_grades SET avg = ( SELECT round( CAST (tc.total_count AS FLOAT) / " + \
          str(counts_counter - 3) + ", 3) "
    sql = sql + "FROM tc WHERE tc.id = test_grades.id)"
    query.exec(sql)
    query.exec("SELECT avg FROM test_grades")
    query.next()
    return query.value(0)


def update_combo():
    query = QSqlQuery()
    query.exec_("SELECT groups FROM test_grades")
    query.next()
    return query.value(0)


def addStudent(a, b):
    query = QSqlQuery()
    query.prepare("INSERT INTO test_grades (groups, name) VALUES (?, ?)")
    query.addBindValue(a)
    query.addBindValue(b)
    query.exec()
    query.prepare("SELECT name FROM test_grades WHERE groups = ?")
    query.addBindValue(a)
    query.exec()
    query.last()

    return query.value(0)

def delStudent(a):
    query = QSqlQuery()
    query.prepare("DELETE FROM test_grades WHERE name = ?")
    query.addBindValue(a)
    query.exec()
    query.exec("DELETE FROM test_progress WHERE test_progress.name NOT IN (SELECT name FROM test_grades)")
    query.prepare("SELECT name FROM test_progress WHERE name = ?")
    query.addBindValue(a)
    query.exec()
    query.first()

    return query.value(0)

def copyFromStudents():
    query = QSqlQuery()
    query.exec_("INSERT INTO test_progress (id, groups, name) SELECT id, groups, name "
                "FROM test_grades WHERE test_grades.id NOT IN (SELECT id FROM test_progress)")
    query.exec("UPDATE test_progress SET groups = (SELECT test_grades.groups FROM test_grades "
               "WHERE test_progress.id = test_grades.id)")
    query.exec("UPDATE test_progress SET name = (SELECT test_grades.name FROM test_grades "
               "WHERE test_progress.id = test_grades.id)")
    query.exec("SELECT name FROM test_grades")
    query.first()
    a = query.value(0)
    query.exec("SELECT name FROM test_progress")
    query.first()
    b = query.value(0)
    if a == b:
        return True
    else:
        return False


def assignAchv(a, b):
    query = QSqlQuery()
    query.prepare("UPDATE test_progress SET ach1 = ? WHERE name = ?")
    query.addBindValue(a)
    query.addBindValue(b)
    query.exec()
    query.prepare("SELECT ach1 FROM test_progress WHERE name = ?")
    query.addBindValue(b)
    query.exec()
    query.first()
    return query.value(0)


def addProgress(a, b):
    query = QSqlQuery()
    query.prepare("INSERT INTO test_achievements (achievement, src) VALUES (?, ?)")
    query.addBindValue(a)
    query.addBindValue(b)
    query.exec()
    query.prepare("SELECT src FROM test_achievements WHERE achievement = ?")
    query.addBindValue(a)
    query.exec()
    query.last()
    return query.value(0)

def addColumn():
    query = QSqlQuery()
    query.exec("ALTER TABLE test_progress ADD COLUMN ach2 INTEGER DEFAULT 0")
    query.exec("SELECT ach2 FROM test_progress")
    query.first()
    return query.value(0)

def delProgress(a):
    query = QSqlQuery()
    query.prepare("DELETE FROM test_achievements WHERE achievement = ?;")
    query.addBindValue(a)
    query.exec()
    query.prepare("SELECT achievement FROM test_achievements WHERE achievement = ?")
    query.addBindValue(a)
    query.exec()
    query.first()
    return query.value(0)

def delColumn(a):
    query = QSqlQuery()
    query.exec("SELECT name FROM test_progress")
    query.last()
    b = query.value(0)

    query.exec("PRAGMA foreign_keys = off")
    query.exec("BEGIN TRANSACTION")
    query.exec("CREATE TABLE IF NOT EXISTS progress_temp (id INTEGER, groups TEXT, name TEXT)")
    query.prepare("SELECT id FROM achievements WHERE achievement = ?")
    query.addBindValue(a)
    query.exec()
    query.next()
    item = query.value(0)
    list = []
    query.exec("SELECT id FROM temp_achievements")

    while query.next():
        if query.value(0) != item:
            list.append(query.value(0))
    list.reverse()
    sql = ""
    for i in list:
        sql = "ALTER TABLE progress_temp ADD COLUMN ach" + str(i) + " INTEGER DEFAULT 0"
        query.exec(sql)

    query.exec("INSERT INTO progress_temp(id, groups, name) SELECT id, groups, name FROM test_progress")
    sql = ""
    for i in list:
        sql = "UPDATE progress_temp SET ach" + str(i) + " = (SELECT test_progress.ach" + str(i) + \
              " FROM test_progress WHERE test_progress.id = progress_temp.id)"
        query.exec(sql)
    query.exec("DROP TABLE test_progress")
    query.exec("ALTER TABLE progress_temp RENAME TO test_progress")
    query.exec("COMMIT")
    query.exec("PRAGMA foreign_keys = on")
    query.exec("SELECT name FROM progress")
    query.last()
    c = query.value(0)
    if b == c:
        return True
    else:
        return False

def showWorks():
    query = QSqlQuery()
    query.exec("INSERT INTO test_works (id, groups, name) "
               "SELECT id, groups, name "
               "FROM test_grades "
               "WHERE test_grades.id NOT IN"
               "(SELECT id FROM test_works)")
    query.exec("DELETE FROM test_works WHERE test_works.id NOT IN (SELECT id FROM test_grades)")
    query.exec("UPDATE test_works SET groups = (SELECT test_grades.groups FROM test_grades "
               "WHERE test_grades.id = test_works.id)")
    query.exec("UPDATE test_works SET name = (SELECT test_grades.name FROM test_grades "
               "WHERE test_grades.id = test_works.id)")

    query.exec("SELECT name FROM test_grades")
    query.last()
    a = query.value(0)
    query.exec("SELECT name FROM test_works")
    query.last()
    b = query.value(0)
    if a == b:
        return True
    else:
        return False

def delWorks():
    query = QSqlQuery()
    query.exec("SELECT name FROM test_works")
    query.last()
    b = query.value(0)
    query.exec("PRAGMA foreign_keys = off")
    query.exec("BEGIN TRANSACTION")
    # Создание новой таблицы works_temp
    query.exec("CREATE TABLE IF NOT EXISTS works_temp ("
               "id INTEGER, "
               "groups TEXT, "
               "name TEXT, "
               "FOREIGN KEY (id) REFERENCES test_grades (id) ON DELETE CASCADE ON UPDATE CASCADE)")
    # Создание столбцов для works_temp
    for i in range(1, 2):
        sql = "ALTER TABLE works_temp ADD COLUMN Practical_" + str(i) + " INTEGER"
        query.exec(sql)
        sql = "ALTER TABLE works_temp ADD COLUMN Deadline_" + str(i) + " INTEGER"
        query.exec(sql)
    # Копирование данных из works в works_temp
    query.exec("INSERT INTO works_temp(id, groups, name) SELECT id, groups, name FROM works")
    for i in range(1, 2):
        sql = "UPDATE works_temp SET Practical_" + str(i) + " = (SELECT test_works.Practical_" + str(i) + \
              " FROM test_works WHERE test_works.id = works_temp.id)"
        query.exec(sql)
        sql = "UPDATE works_temp SET Deadline_" + str(i) + " = (SELECT test_works.Deadline_" + str(i) + \
              " FROM test_works WHERE test_works.id = works_temp.id)"
        query.exec(sql)
    query.exec("DROP TABLE test_works")
    query.exec("ALTER TABLE works_temp RENAME TO test_works")
    query.exec("COMMIT")
    query.exec("PRAGMA foreign_keys = on")
    query.exec("SELECT name FROM test_works")
    query.last()
    c = query.value(0)
    if b == c:
        return True
    else:
        return False


class TestStringMethods(unittest.TestCase):


    def setUp(self):
        init_test_table()

    def tearDown(self):
        remove_test_table()

    @unittest.skip('Not supported in Gitlab')
    def test_avg(self):
        self.assertEqual(avg(), 4)

    @unittest.skip('Not supported in Gitlab')
    def test_updateCombo(self):
        self.assertEqual(update_combo(), "КИ18-17Б")

    @unittest.skip('Not supported in Gitlab')
    def test_addStudents(self):

        self.assertEqual(addStudent('КИ18-16Б', 'Малахитов'), "Малахитов")
        self.assertEqual(addStudent('КИ18-16Б', 'Маккофе'), "Маккофе")
        self.assertEqual(addStudent('КИ18-16Б', 'Гоголь'), "Гоголь")

    def test_delStudent(self):

        self.assertEqual(delStudent('Петров'), None)
        self.assertEqual(delStudent('Ведров'), None)
        self.assertEqual(delStudent('Миксеров'), None)

    def test_copyFromStudent(self):

        self.assertTrue(copyFromStudents())

    @unittest.skip('Not supported in Gitlab')
    def test_assign(self):

        self.assertEqual(assignAchv(2, 'Алексеев'), 2)
        self.assertEqual(assignAchv(1, 'Алексеев'), 1)
        self.assertEqual(assignAchv(0, 'Васильев'), 0)

    @unittest.skip('Not supported in Gitlab')
    def test_addProgress(self):

        self.assertEqual(addProgress('ачивка', 'horosh.jpg'), "horosh.jpg")
        self.assertEqual(addProgress('качико', 'preich.jpg'), "preich.jpg")
        self.assertEqual(addProgress('кореич', 'papech.jpg'), "papech.jpg")

    @unittest.skip('Not supported in Gitlab')
    def test_addColumn(self):

        self.assertEqual(addColumn(), 0)

    def test_delProgress(self):

        self.assertEqual(delProgress('качико'), None)
        self.assertEqual(delProgress('ачивка'), None)
        self.assertEqual(delProgress('кореич'), None)

    def test_delColumn(self):

        self.assertTrue(delColumn('ачивка3'))
        self.assertTrue(delColumn('ачивка1'))
        self.assertTrue(delColumn('ачивка2'))

    def test_showWorks(self):

        self.assertTrue(showWorks())

    def test_delWork(self):

        self.assertTrue(delWorks())

    def test_DB(self):

        self.assertEqual(DB.initDB(self), 0)


if __name__ == '__main__':
    unittest.main()