from cx_Freeze import setup, Executable

setup(
    name = "Grades",
    version = "0.1",
    description = "Grades",
    executables = [Executable("main.py")]
)