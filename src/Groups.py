from PyQt5.QtWidgets import *
from PyQt5.QtSql import QSqlQueryModel, QSqlQuery, QSqlTableModel
from PyQt5.QtCore import *
from PyQt5.QtGui import QIcon
from math import floor

class Groups(QWidget):
    def __init__(self):
        super().__init__()
        self.field = "Группа:"
        self.initTable()
        self.showData()
        self.binds()

    def initTable(self):
        layoutGrid = QGridLayout()
        self.setLayout(layoutGrid)
        comboLayout = QHBoxLayout()
        btnLayout = QHBoxLayout()
        self.label = QLabel(self.field, self)
        self.combo = QComboBox(self)
        self.combo.setFixedSize(150, 25)
        self.table = QTableView(self)
        self.table.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.btn_add = QPushButton(self)
        self.btn_add.setToolTip('Добавить студента')
        self.btn_add.setIcon(QIcon('images/add.png'))
        self.btn_del = QPushButton(self)
        self.btn_del.setToolTip('Удалить студента')
        self.btn_del.setIcon(QIcon('images/remove.png'))
        self.btn_refresh = QPushButton("Обновить", self)
        self.btn_refresh.setIcon(QIcon('images/refresh.png'))
        comboLayout.addWidget(self.label, Qt.AlignLeft)
        comboLayout.addWidget(self.combo, 15, Qt.AlignLeft)
        comboLayout.addWidget(self.btn_refresh, 60, Qt.AlignLeft)
        btnLayout.addWidget(self.btn_add)
        btnLayout.addWidget(self.btn_del)
        comboLayout.setSpacing(0)
        self.table.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        layoutGrid.addItem(comboLayout, 0, 0)
        layoutGrid.addItem(btnLayout, 0, 1)
        layoutGrid.addWidget(self.table, 1, 0, 1, 0)

    def showData(self):
        query = QSqlQuery()
        query.exec("SELECT * FROM works LIMIT 1")
        i = 0
        query.next()
        while (query.value(i) != None):
            i = i + 1
        columns_counter = i
        if (columns_counter == 3):
            query.exec_("UPDATE grades SET avg = 0")
        else:
            sql = "WITH tc AS ( SELECT ( "
            for i in range(3, columns_counter, 3):
                sql = sql + 'IFNULL(Practical_' + str(floor(i / 3)) + ', 0) + IFNULL(Deadline_' + str(floor(i / 3)) + ', 0)'
                if (i + 3) < columns_counter:
                    sql = sql + ' + '
            sql = sql + ") AS total_count, id FROM works) "
            sql = sql + "UPDATE grades SET avg = ( SELECT round( CAST (tc.total_count AS FLOAT) / " + \
                  str(columns_counter - ((columns_counter / 3) + 2)) + ", 3) "
            sql = sql + "FROM tc WHERE tc.id = grades.id)"
            query.exec_(sql)

        self.model = QSqlTableModel()
        self.model.setTable("grades")
        self.model.setEditStrategy(QSqlTableModel.OnFieldChange)
        self.model.select()
        self.model.setHeaderData(1, Qt.Horizontal, 'Группа')
        self.model.setHeaderData(2, Qt.Horizontal, 'ФИО')
        self.model.setHeaderData(3, Qt.Horizontal, 'Ср.балл')
        self.table.setModel(self.model)
        self.table.sortByColumn(2, Qt.AscendingOrder)
        self.table.setSortingEnabled(True)
        self.table.setColumnWidth(1, 90)
        self.table.setColumnWidth(2, 300)
        self.table.hideColumn(0)
        self.updateCombo()

    def updateCombo(self):
        self.listOfGroups = []
        query = QSqlQuery()
        query.exec_("SELECT groups FROM grades")
        while query.next():
            self.listOfGroups.append(query.value(0))
        self.listOfGroups = list(set(self.listOfGroups))
        self.listOfGroups.sort()
        self.listOfGroups.insert(0, "Все")
        currentItem = self.combo.currentText()
        self.combo.clear()
        for i in self.listOfGroups:
            self.combo.addItem(i)
        self.combo.setCurrentText(currentItem)
        self.filter()

    def binds(self):
        self.btn_add.clicked.connect(self.add)
        self.btn_del.clicked.connect(self.delete)
        self.combo.currentIndexChanged.connect(self.filter)
        self.btn_refresh.clicked.connect(self.refresh)

    @pyqtSlot()
    def add(self):
        group = str(self.combo.currentText())
        if group == "Все":
            group = ""
        query = QSqlQuery()
        query.prepare("INSERT INTO grades (groups, name, avg) VALUES (?, ?, 0)")
        query.addBindValue(group)
        query.addBindValue("")
        query.exec_()
        self.model.setFilter("")
        self.updateCombo()

    @pyqtSlot()
    def delete(self):
        self.model.removeRow(self.table.currentIndex().row())
        self.model.select()
        query = QSqlQuery()
        query.exec_("DELETE FROM progress WHERE progress.id NOT IN (SELECT id FROM grades)")
        self.updateCombo()

    @pyqtSlot()
    def filter(self):
        if self.combo.currentText() == "Все" or self.combo.currentText() == "":
            self.model.setFilter("")
        else:
            currentFilter = "groups == '" + self.combo.currentText() + "'"
            self.model.setFilter(currentFilter)
            self.model.select()

    @pyqtSlot()
    def refresh(self):
        self.showData()



