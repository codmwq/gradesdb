from PyQt5.QtWidgets import *
from PyQt5.QtSql import QSqlQuery, QSqlTableModel
from PyQt5.QtCore import *
from PyQt5.QtGui import QFont, QIcon
from PyQt5 import QtWidgets
from math import floor
from PyQt5.QtSql import *
from array import *



class Works(QWidget):
    def __init__(self):
        super().__init__()
        self.field = "Группа:"
        self.initTable()
        self.showData()
        self.binds()

    def initTable(self):
        layoutGrid = QGridLayout()
        self.setLayout(layoutGrid)
        comboLayout = QHBoxLayout()
        btnLayout = QHBoxLayout()
        self.label = QLabel(self.field, self)
        self.combo = QComboBox(self)
        self.combo.setFixedSize(150, 25)
        self.table = QTableView(self)
        self.table.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.btn_add = QPushButton(self)
        self.btn_add.setToolTip('Добавить практическую')
        self.btn_add.setIcon(QIcon('images/add.png'))
        self.btn_del = QPushButton(self)
        self.btn_del.setIcon(QIcon('images/remove.png'))
        self.btn_del.setToolTip('Удалить практическую')
        self.btn_refresh = QPushButton("Обновить", self)
        self.btn_refresh.setIcon(QIcon('images/refresh.png'))
        self.btn_deadline = QPushButton("Дедлайны", self)
        self.btn_deadline.setIcon(QIcon('images/deadlines.png'))
        comboLayout.addWidget(self.label, Qt.AlignLeft)
        comboLayout.addWidget(self.combo, 15, Qt.AlignLeft)
        comboLayout.addWidget(self.btn_refresh, 60, Qt.AlignLeft)
        comboLayout.addWidget(self.btn_deadline, 0, Qt.AlignLeft)
        btnLayout.addWidget(self.btn_add)
        btnLayout.addWidget(self.btn_del)
        comboLayout.setSpacing(1)
        self.table.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        layoutGrid.addItem(comboLayout, 0, 0)
        layoutGrid.addItem(btnLayout, 0, 1)
        layoutGrid.addWidget(self.table, 1, 0, 1, 0)


    def showData(self):
        group = str(self.combo.currentText())
        query = QSqlQuery()
        query.exec("INSERT INTO works (id, groups, name) "
                   "SELECT id, groups, name "
                   "FROM grades "
                   "WHERE grades.id NOT IN"
                   "(SELECT id FROM works)")
        query.exec("DELETE FROM works WHERE works.id NOT IN (SELECT id FROM grades)")
        query.exec("UPDATE works SET groups = (SELECT grades.groups FROM grades WHERE grades.id = works.id)")
        query.exec("UPDATE works SET name = (SELECT grades.name FROM grades WHERE grades.id = works.id)")
        query = QSqlQuery()
        query.exec("SELECT * FROM works LIMIT 1")
        i = 0
        query.next()
        while (query.value(i) != None):
            i = i + 1
        counts_counter = i
        for i in range(3, counts_counter, 3):
            query.exec("UPDATE works SET Date_" + str(floor(i / 3)) + " = (SELECT Date_" + str(floor(i / 3)) +
                       " FROM works WHERE Date_" + str(floor(i / 3)) + " IS NOT NULL) WHERE Date_" + str(floor(i / 3)) +
                       " IS NULL")

        self.model = QSqlTableModel()
        self.model.setTable("works")
        self.model.setEditStrategy(QSqlTableModel.OnFieldChange)
        self.model.select()
        self.table.setModel(self.model)
        self.table.sortByColumn(2, Qt.AscendingOrder)
        self.table.setSortingEnabled(True)
        self.table.setColumnWidth(1, 90)
        self.table.setColumnWidth(2, 300)
        for i in range(3, self.model.columnCount()):
            self.table.setColumnWidth(i, 40)
        self.model.setHeaderData(1, Qt.Horizontal, 'Группа')
        self.model.setHeaderData(2, Qt.Horizontal, 'ФИО')
        for i in range(3, self.model.columnCount(), 3):
            self.model.setHeaderData(i, Qt.Horizontal, 'П' + str(floor(i / 3)))
            self.model.setHeaderData(i + 1, Qt.Horizontal, 'Д' + str(floor(i / 3)))
            self.table.hideColumn(i + 2)
        self.table.hideColumn(0)

        for i in range(3, self.model.columnCount() + 3, 3):
            query.exec("DROP TRIGGER IF EXISTS auto_deadline" + str(floor(i / 3)))
        if (self.model.columnCount() > 3):
            for i in range(3, self.model.columnCount(), 3):
                sql = "CREATE TRIGGER auto_deadline" + str(floor(i / 3)) + " AFTER UPDATE OF "
                sql = sql + "Practical_" + str(floor(i / 3))
                sql = sql + " ON works "
                sql = sql + "WHEN (SELECT count(*) FROM works WHERE Practical_" + str(floor(i / 3)) + \
                      " = NEW.Practical_" + str(floor(i / 3)) + ") > 0 "
                sql = sql + " BEGIN UPDATE works SET Deadline_" + str(floor(i / 3)) + " = "
                sql = sql + "(SELECT CASE WHEN (strftime('%j','now') - strftime('%j', tc.Date_"\
                      + str(floor(i / 3)) + ")) <= 0 "
                sql = sql + "THEN 5 " \
                            "WHEN (strftime('%j','now') - strftime('%j', tc.Date_" + str(floor(i / 3)) + ")) > 56 "
                sql = sql + "THEN 0 " \
                            "ELSE (5 - (strftime('%j','now') - strftime('%j', tc.Date_" \
                      + str(floor(i / 3)) + ") + 13) / 14) "
                sql = sql + "END FROM (SELECT works.Practical_" + str(floor(i / 3)) + ", works.Date_"\
                            + str(floor(i / 3)) + " FROM works WHERE works.Practical_" + str(floor(i / 3)) + " = " \
                            + "NEW.Practical_" + str(floor(i / 3)) + ") AS tc) " \
                            "WHERE (Deadline_" + str(floor(i / 3)) + " IS NULL OR Deadline_" + str(floor(i / 3)) + " = '')" \
                            " AND (Practical_" \
                            + str(floor(i / 3)) + " IS NOT NULL AND Practical_" + str(floor(i / 3)) + " != ''); "
                sql = sql + "END;"
                query.exec(sql)

        self.updateCombo()

    def updateCombo(self):
        self.listOfGroups = []
        query = QSqlQuery()
        query.exec_("SELECT groups FROM grades")
        while query.next():
            self.listOfGroups.append(query.value(0))
        self.listOfGroups = list(set(self.listOfGroups))
        self.listOfGroups.sort()
        self.listOfGroups.insert(0, "Все")
        currentItem = self.combo.currentText()
        self.combo.clear()
        for i in self.listOfGroups:
            self.combo.addItem(i)
        self.combo.setCurrentText(currentItem)


    def binds(self):
        self.btn_add.clicked.connect(self.add)
        self.btn_del.clicked.connect(self.delete)
        self.combo.currentIndexChanged.connect(self.filter)
        self.btn_refresh.clicked.connect(self.refresh)
        self.btn_deadline.clicked.connect(self.deadlines)

    @pyqtSlot()
    def add(self):
        dialog = Calendar()
        dialog.exec_()
        if (dialog.date != None):
            result = floor(self.model.columnCount() / 3)
            query = QSqlQuery()
            sql = "ALTER TABLE works ADD COLUMN Practical_" + str(result) + " INTEGER"
            query.exec(sql)
            sql = "ALTER TABLE works ADD COLUMN Deadline_" + str(result) + " INTEGER"
            query.exec(sql)
            sql = "ALTER TABLE works ADD COLUMN Date_" + str(result) + " TEXT"
            query.exec(sql)
            year = QDate.year(dialog.date)
            month = QDate.month(dialog.date)
            day = QDate.day(dialog.date)
            sql = str(year) + "-"
            if month < 10:
                sql = sql + '0' + str(month) + "-"
            else:
                sql = sql + str(month) + "-"
            if day < 10:
                sql = sql + '0' + str(day)
            else:
                sql = sql + str(day)
            query = QSqlQuery()
            query.prepare("UPDATE works SET Date_" + str(result) + " = ?")
            query.addBindValue(sql)
            query.exec()
        self.showData()
        self.model.setFilter("")

    @pyqtSlot()
    def delete(self):
        index = self.table.currentIndex().column()
        if (index >= 3):
            msgBox = QMessageBox()
            msgBox.setIcon(QMessageBox.Information)
            msgBox.setText("Вы действительно хотите удалить данные о " + str(floor(index / 3))
                           + " практической?")
            msgBox.setWindowTitle("Подтверждение")
            msgBox.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)

            returnValue = msgBox.exec()
            if returnValue == QMessageBox.Ok:
                query = QSqlQuery()
                query.exec("PRAGMA foreign_keys = off")
                query.exec("BEGIN TRANSACTION")
                # Создание новой таблицы works_temp
                query.exec("CREATE TABLE IF NOT EXISTS works_temp ("
                           "id INTEGER, "
                           "groups TEXT, "
                           "name TEXT, "
                           "FOREIGN KEY (id) REFERENCES grades (id) ON DELETE CASCADE ON UPDATE CASCADE)")
                # Создание столбцов для works_temp которые были перед удаляемым столбцом
                query.exec("INSERT INTO works_temp(id, groups, name) SELECT id, groups, name FROM works")
                for i in range(1, floor(index / 3)):
                    sql = "ALTER TABLE works_temp ADD COLUMN Practical_" + str(i) + " INTEGER"
                    query.exec(sql)
                    sql = "UPDATE works_temp SET Practical_" + str(i) + " = (SELECT works.Practical_" + str(i) + \
                          " FROM works WHERE works.id = works_temp.id)"
                    query.exec(sql)
                    sql = "ALTER TABLE works_temp ADD COLUMN Deadline_" + str(i) + " INTEGER"
                    query.exec(sql)
                    sql = "UPDATE works_temp SET Deadline_" + str(i) + " = (SELECT works.Deadline_" + str(i) + \
                          " FROM works WHERE works.id = works_temp.id)"
                    query.exec(sql)
                    sql = "ALTER TABLE works_temp ADD COLUMN Date_" + str(i) + " TEXT"
                    query.exec(sql)
                    sql = "UPDATE works_temp SET Date_" + str(i) + " = (SELECT works.Date_" + str(i) + \
                          " FROM works WHERE works.id = works_temp.id)"
                    query.exec(sql)

                # Создание столбцов для works_temp которые были после удаляемого столбца
                for i in range(floor(index / 3) + 1, floor(self.model.columnCount() / 3)):
                    sql = "ALTER TABLE works_temp ADD COLUMN Practical_" + str(i - 1) + " INTEGER"
                    query.exec(sql)
                    sql = "UPDATE works_temp SET Practical_" + str(i - 1) + " = (SELECT works.Practical_" + str(i) + \
                          " FROM works WHERE works.id = works_temp.id)"
                    query.exec(sql)

                    sql = "ALTER TABLE works_temp ADD COLUMN Deadline_" + str(i - 1) + " INTEGER"
                    query.exec(sql)
                    sql = "UPDATE works_temp SET Deadline_" + str(i - 1) + " = (SELECT works.Deadline_" + str(i) + \
                          " FROM works WHERE works.id = works_temp.id)"
                    query.exec(sql)
                    sql = "ALTER TABLE works_temp ADD COLUMN Date_" + str(i - 1) + " TEXT"
                    query.exec(sql)
                    sql = "UPDATE works_temp SET Date_" + str(i - 1) + " = (SELECT works.Date_" + str(i) + \
                          " FROM works WHERE works.id = works_temp.id)"
                    query.exec(sql)
                query.exec("DROP TABLE works")
                query.exec("ALTER TABLE works_temp RENAME TO works")
                query.exec("COMMIT")
                query.exec("PRAGMA foreign_keys = on")
        self.showData()

    @pyqtSlot()
    def filter(self):
        if self.combo.currentText() == "Все" or self.combo.currentText() == "":
            self.model.setFilter("")
        else:
            currentFilter = "groups == '" + self.combo.currentText() + "'"
            self.model.setFilter(currentFilter)
            self.model.select()

    @pyqtSlot()
    def refresh(self):
        self.showData()

    @pyqtSlot()
    def deadlines(self):
        query = QSqlQuery()
        query.exec("SELECT count(*) FROM works")
        query.next()
        if (query.value(0) == 0):
            QMessageBox.warning(self, "Ошибка", "Добавьте для начала студентов")
        elif (self.model.columnCount() == 3):
            QMessageBox.warning(self, "Ошибка", "Добавьте для начала практические работы")
        else:
            dialog = Deadlines()
            dialog.exec_()
        self.showData()


class Calendar(QDialog):

    def __init__(self):
        super().__init__()
        self.initUI()
        self.binds()

    def initUI(self):
        query = QSqlQuery()
        query.exec("SELECT * FROM works LIMIT 1")
        i = 0
        query.next()
        while (query.value(i) != None):
            i = i + 1
        columns_counter = i

        self.my_calendar = QCalendarWidget(self)
        self.my_calendar.setGridVisible(True)
        self.my_calendar.move(10, 50)
        self.date = None

        text = "Выберите дату дедлайна для " + str(floor(columns_counter / 3)) +" работы "
        self.information = QLabel(text, self)
        self.information.setFont(QFont("Arial", 9))


        self.btn_ok = QPushButton("Выбрать", self)
        self.btn_cancel = QPushButton("Отмена", self)

        self.information.move(20, 20)
        self.btn_cancel.move(300, 300)
        self.btn_ok.move(200, 300)
        self.setGeometry(850, 350, 410, 350)
        self.setWindowTitle('Calendar')
        self.show()

    def binds(self):
        self.btn_ok.clicked.connect(self.choice)
        self.btn_cancel.clicked.connect(self.close)

    def choice(self):
        self.date = self.my_calendar.selectedDate()
        self.close()

class Deadlines(QDialog):

    def __init__(self):
        super().__init__()
        self.count_dead = 0
        self.initUI()
        self.binds()


    def initUI(self):
        query = QSqlQuery()

        query.exec("SELECT * FROM works LIMIT 1")
        i = 0
        query.next()
        while (query.value(i) != None):
            i = i + 1
        columns_counter = i

        self.count_dead = (floor((columns_counter / 3)) - 1)

        self.setWindowTitle('Deadline list')
        self.setGeometry(850, 350, 410, 350)
        self.label = QLabel("Выберите: ", self)
        self.label.setFont(QFont("Helvetica", 8))
        self.label.move(20, 16)
        self.box = QComboBox(self)
        self.box.setFixedWidth(200)
        self.box.move(90, 14)
        self.btn_change = QPushButton("Изменить", self)
        self.btn_change.move(300, 12)
        self.my_calendar = QCalendarWidget(self)
        self.my_calendar.setGridVisible(True)
        self.my_calendar.move(10, 50)
        self.btn_success = QPushButton("Готово", self)
        self.btn_success.move(300, 300)
        self.updateCombo()

    def updateCombo(self):
        currentItem = self.box.currentIndex()
        if (currentItem == -1):
            currentItem += 1
        self.box.blockSignals(True)
        self.box.clear()
        query = QSqlQuery()
        for i in range(0, self.count_dead):
            query.exec("SELECT Date_" + str(i + 1) + " FROM works WHERE Date_" + str(i + 1) + " IS NOT NULL LIMIT 1")
            query.next()
            text = str(i + 1) + " Практическая: " + str(query.value(0))
            self.box.addItem(text)
        self.box.blockSignals(False)
        self.box.setCurrentIndex(currentItem)

        sql = "SELECT Date_" + str(currentItem + 1) + " FROM works WHERE Date_" + \
              str(currentItem + 1) + " IS NOT NULL LIMIT 1"
        query.exec(sql)
        query.next()
        if (query.value(0) != None):
            date = str(query.value(0))
            year = int(date[0] + date[1] + date[2] + date[3])
            month = int(date[5] + date[6])
            day = int(date[8] + date[9])
            self.my_calendar.setSelectedDate(QDate(year, month, day))
        else:
            self.my_calendar.showToday()

    def binds(self):
        self.btn_success.clicked.connect(self.success)
        self.box.currentIndexChanged.connect(self.filter)
        self.btn_change.clicked.connect(self.change)

    @pyqtSlot()
    def success(self):
        self.close()

    @pyqtSlot()
    def filter(self):
        currentItem = self.box.currentIndex()
        query = QSqlQuery()
        query.exec("SELECT Date_" + str(currentItem + 1) + " FROM works WHERE Date_" + str(currentItem + 1) +
                   " IS NOT NULL LIMIT 1")
        query.next()
        if (query.value(0) != None):
            date = str(query.value(0))
            year = int(date[0] + date[1] + date[2] + date[3])
            month = int(date[5] + date[6])
            day = int(date[8] + date[9])
            self.my_calendar.setSelectedDate(QDate(year, month, day))
        else:
            self.my_calendar.showToday()

    @pyqtSlot()
    def change(self):
        currentItem = self.box.currentIndex()
        date = self.my_calendar.selectedDate()
        year = QDate.year(date)
        month = QDate.month(date)
        day = QDate.day(date)
        sql = str(year) + "-"
        if month < 10:
            sql = sql + '0' + str(month) + "-"
        else:
            sql = sql + str(month) + "-"
        if day < 10:
            sql = sql + '0' + str(day)
        else:
            sql = sql + str(day)
        query = QSqlQuery()
        query.exec("UPDATE works SET Date_" + str(currentItem + 1) +" = '" + sql + "' WHERE id > 0")
        self.updateCombo()
