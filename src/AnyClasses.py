from PyQt5.QtWidgets import QMessageBox

class MessageBox():
    def __init__(self, title, text):
        super().__init__()
        self.initMB(title, text)

    def initMB(self, title, text):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle(title)
        msg.setText(text)
        okButton = msg.addButton('OK', QMessageBox.AcceptRole)
        msg.exec()
