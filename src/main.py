from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import pyqtSlot, QSize, Qt
from PyQt5.QtSql import QSqlQueryModel, QSqlDatabase
import sys
from src.DataBase import *
from src.Groups import Groups
from src.Progress import Progress
from src.Works import Works

class App(QWidget):
    def __init__(self):
        super().__init__()
        self.title = 'Grades'
        self.left = 500
        self.top = 150
        self.width = 1200
        self.height = 700
        self.initUI()
        self.binds()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setWindowIcon(QIcon('images/logo.png'))
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.stacked = QStackedWidget(self)
        self.stacked.addWidget(Groups())
        self.stacked.addWidget(Progress())
        self.stacked.addWidget(Works())
        buttonLayout = QVBoxLayout()
        buttonLayout.setContentsMargins(0, 0, 0, 400)
        layoutGrid = QGridLayout()
        self.setLayout(layoutGrid)
        self.stacked.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btn_group = QPushButton("Группы", self)
        self.btn_prog = QPushButton("Достижения", self)
        self.btn_work = QPushButton("Практические", self)
        self.setImg()
        buttonLayout.addWidget(self.btn_group)
        buttonLayout.addWidget(self.btn_prog)
        buttonLayout.addWidget(self.btn_work)
        layoutGrid.addWidget(self.stacked, 0, 1)
        layoutGrid.addItem(buttonLayout, 0, 0)


    def setImg(self):
        self.btn_group.setIcon(QIcon('images/groups.png'))
        self.btn_group.setIconSize(QSize(50, 50))
        self.btn_group.setObjectName('BtnMenu')
        self.btn_prog.setIcon(QIcon('images/progress.png'))
        self.btn_prog.setIconSize(QSize(50, 50))
        self.btn_prog.setObjectName('BtnMenu')
        self.btn_work.setIcon(QIcon('images/works.png'))
        self.btn_work.setIconSize(QSize(50, 50))
        self.btn_work.setObjectName('BtnMenu')

    def binds(self):
        self.btn_group.clicked.connect(self.groupTable)
        self.btn_prog.clicked.connect(self.progTable)
        self.btn_work.clicked.connect(self.workTable)

    def groupTable(self):
        self.stacked.setCurrentIndex(0)


    def progTable(self):
        self.stacked.setCurrentIndex(1)


    def workTable(self):
        self.stacked.setCurrentIndex(2)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    db = DB()
    if db.initDB() == 1:
        sys.exit(app.exec_())
    ex = App()
    stylesheet = open('style.css')
    style = stylesheet.read()
    ex.setStyleSheet(style)
    ex.show()
    sys.exit(app.exec_())