from PyQt5.QtSql import QSqlDatabase
from src.AnyClasses import MessageBox


class DB:
    def __init__(self):
        super().__init__()
        self.initDB()

    def initDB(self):
        db_connect = QSqlDatabase.addDatabase('QSQLITE')
        db_connect.setDatabaseName('mybase.db')
        if not db_connect.open():
            print('error')
            MessageBox(("Ошибка", "Ошибка соединения с базой данных.\nНажмите ОК для продолжения"))
            return 1
        return 0
