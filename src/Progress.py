from PyQt5.QtWidgets import *
from PyQt5.QtSql import QSqlQueryModel, QSqlQuery
from PyQt5.QtCore import *
from PyQt5.QtGui import QIcon
from src.AnyClasses import MessageBox
from shutil import copyfile
from os import path, remove


class Progress(QWidget):
    def __init__(self):
        super().__init__()
        self.currentAch = ""
        self.field = "Группа:"
        self.listOfProgress = []
        self.initTable()
        self.showTable()
        self.initProgress()
        self.binds()

    def initTable(self):
        layoutGrid = QGridLayout()
        layoutGrid.setSpacing(20)
        self.setLayout(layoutGrid)
        comboLayout = QHBoxLayout()
        btnLayout = QHBoxLayout()
        self.label = QLabel(self.field, self)
        self.combo = QComboBox(self)
        self.combo.setFixedSize(150, 25)
        self.table = QTableView(self)
        self.table.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.btn_add = QPushButton(self)
        self.btn_add.setToolTip('Добавить достижение')
        self.btn_add.setIcon(QIcon('images/add.png'))
        self.btn_del = QPushButton(self)
        self.btn_del.setToolTip('Удалить достижение')
        self.btn_del.setIcon(QIcon('images/remove.png'))
        self.btn_refresh = QPushButton("Обновить", self)
        self.btn_refresh.setIcon(QIcon('images/refresh.png'))
        self.assign_group = QGroupBox("Присвоить достижение",self)
        self.lay_assign = QVBoxLayout()
        self.btn_assign = QPushButton("Присвоить", self)
        self.radio_received = QRadioButton("Полученное", self)
        self.radio_waiting = QRadioButton("Невыданное", self)
        self.radio_uncollected = QRadioButton("Неполученное", self)
        self.lay_assign.addWidget(self.radio_received)
        self.lay_assign.addWidget(self.radio_waiting)
        self.lay_assign.addWidget(self.radio_uncollected)
        self.lay_assign.addWidget(self.btn_assign)
        self.assign_group.setLayout(self.lay_assign)

        lists_layout = QVBoxLayout()
        self.lbl_received = QLabel("Полученные:", self)
        self.lbl_uncollected = QLabel("Неполученные:", self)
        self.lbl_waiting = QLabel("Ожидающие выдачи:", self)
        self.list_received = QListWidget(self)
        self.list_uncollected = QListWidget(self)
        self.list_waiting = QListWidget(self)
        self.list_uncollected.setIconSize(QSize(120, 120))
        self.list_waiting.setIconSize(QSize(120, 120))
        self.list_received.setIconSize(QSize(120, 120))
        lists_layout.addWidget(self.lbl_received)
        lists_layout.addWidget(self.list_received)
        lists_layout.addWidget(self.lbl_waiting)
        lists_layout.addWidget(self.list_waiting)
        lists_layout.addWidget(self.lbl_uncollected)
        lists_layout.addWidget(self.list_uncollected)
        comboLayout.addWidget(self.label, Qt.AlignLeft)
        comboLayout.addWidget(self.combo, 15, Qt.AlignLeft)
        comboLayout.addWidget(self.btn_refresh, 60, Qt.AlignLeft)
        #btnLayout.addWidget(self.btn_assign)
        btnLayout.addWidget(self.btn_add)
        btnLayout.addWidget(self.btn_del)
        comboLayout.setSpacing(0)
        self.table.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        self.table.setMaximumWidth(400)
        self.table.setMinimumWidth(400)
        layoutGrid.addItem(comboLayout, 0, 0)
        layoutGrid.addItem(btnLayout, 0, 2)
        layoutGrid.addWidget(self.table, 1, 0, 3, 0)
        layoutGrid.addWidget(self.assign_group, 1, 1, 1, 1)
        layoutGrid.addItem(lists_layout, 1, 2, 3, 1)

    def showTable(self):
        query = QSqlQuery()
        query.exec_("INSERT INTO progress (id, groups, name) SELECT id, groups, name "
                   "FROM grades WHERE grades.id NOT IN (SELECT id FROM progress)")
        query.exec("UPDATE progress SET groups = (SELECT grades.groups FROM grades WHERE progress.id = grades.id)")
        query.exec("UPDATE progress SET name = (SELECT grades.name FROM grades WHERE progress.id = grades.id)")
        self.model = QSqlQueryModel()
        self.model.setQuery("SELECT groups, name FROM grades")
        self.model.setHeaderData(0, Qt.Horizontal, 'Группа')
        self.model.setHeaderData(1, Qt.Horizontal, 'ФИО')
        self.table.setModel(self.model)
        self.refresh()

    def initProgress(self):
        query = QSqlQuery()
        self.listOfProgress = []
        query.exec("SELECT * FROM achievements")
        while query.next():
            list = [query.value(0), query.value(1), query.value(2), query.value(3)]
            self.listOfProgress.append(list)
        print(self.listOfProgress)

    def binds(self):
        self.btn_add.clicked.connect(self.addProgress)
        self.btn_del.clicked.connect(self.delProgress)
        self.btn_assign.clicked.connect(self.assignAchv)
        self.list_uncollected.itemClicked.connect(self.focusOnUncollected)
        self.list_waiting.itemClicked.connect(self.focusOnWaiting)
        self.list_received.itemClicked.connect(self.focusOnReceived)
        self.btn_refresh.clicked.connect(self.refresh)
        self.combo.currentIndexChanged.connect(self.filter)
        self.table.clicked.connect(self.showProgress)

    def refresh(self):
        self.listOfGroups = []
        query = QSqlQuery()
        query.exec_("SELECT groups FROM grades")
        while query.next():
            self.listOfGroups.append(query.value(0))
        self.listOfGroups = list(set(self.listOfGroups))
        self.listOfGroups.sort()
        self.listOfGroups.insert(0, "Все")
        currentItem = self.combo.currentText()
        self.combo.clear()
        for i in self.listOfGroups:
            self.combo.addItem(i)
        self.combo.setCurrentText(currentItem)
        self.filter()

    def filter(self):
        if self.combo.currentText() == "Все" or self.combo.currentText() == "":
            self.model.setQuery("SELECT groups, name FROM grades")
        else:
            currentFilter = "SELECT groups, name FROM grades " \
                            "WHERE groups = '" + self.combo.currentText() + "'"
            self.model.setQuery(currentFilter)



    def focusOnUncollected(self):
        try:
            self.currentAch = self.list_uncollected.currentItem().text()
            self.list_waiting.currentItem().setSelected(False)
            self.list_received.currentItem().setSelected(False)
        except AttributeError:
            return 1

    def focusOnWaiting(self):
        try:
            self.currentAch = self.list_waiting.currentItem().text()
            self.list_uncollected.currentItem().setSelected(False)
            self.list_received.currentItem().setSelected(False)
        except AttributeError:
            return 1

    def focusOnReceived(self):
        try:
            self.currentAch = self.list_received.currentItem().text()
            self.list_uncollected.currentItem().setSelected(False)
            self.list_waiting.currentItem().setSelected(False)
        except AttributeError:
            return 1

    def addProgress(self):
        addprog = AddProgress()
        addprog.exec_()
        self.initProgress()

    def delProgress(self):
        delprog = DelProgress()
        delprog.exec_()
        self.initProgress()

    def assignAchv(self):
        student = self.model.record(self.table.currentIndex().row()).value("name")
        print(student)
        print(self.currentAch)
        radio = -1
        if self.radio_uncollected.isChecked():
            radio = 0
        elif self.radio_waiting.isChecked():
            radio = 1
        elif self.radio_received.isChecked():
            radio = 2
        item = 0
        for i in range(0, len(self.listOfProgress)):
            if self.listOfProgress[i][1] == self.currentAch:
                item = self.listOfProgress[i][0]
        query = QSqlQuery()
        query.prepare("UPDATE progress SET ach" + str(item) + "= ? WHERE name = ?")
        query.addBindValue(radio)
        query.addBindValue(student)
        query.exec()
        self.showProgress()

    def showProgress(self):
        student = self.model.record(self.table.currentIndex().row()).value("name")
        self.list_uncollected.clear()
        self.list_waiting.clear()
        self.list_received.clear()
        query = QSqlQuery()
        test = query.exec("SELECT * FROM progress")
        pth = path.dirname(path.abspath(__file__))
        while query.next():
            for i in range(0, len(self.listOfProgress)):
                if query.value(2) == student:
                    if query.value(i + 3) == 0:
                        item = QListWidgetItem(QIcon(pth + self.listOfProgress[i][2]), self.listOfProgress[i][1])
                        item.setToolTip(self.listOfProgress[i][3])
                        self.list_uncollected.addItem(item)
                    if query.value(i + 3) == 1:
                        item = QListWidgetItem(QIcon(pth + self.listOfProgress[i][2]), self.listOfProgress[i][1])
                        item.setToolTip(self.listOfProgress[i][3])
                        self.list_waiting.addItem(item)
                    if query.value(i + 3) == 2:
                        item = QListWidgetItem(QIcon(pth + self.listOfProgress[i][2]), self.listOfProgress[i][1])
                        item.setToolTip(self.listOfProgress[i][3])
                        self.list_received.addItem(item)




class AddProgress(QDialog):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.binds()

    def initUI(self):
        self.setWindowTitle("Добавить достижение")
        self.setGeometry(800, 300, 400, 400)
        self.setMaximumSize(400, 400)
        self.setMinimumSize(400, 400)
        self.lbl_name = QLabel("Название достижения:", self)
        self.lbl_name.move(40, 10)
        self.line_name = QLineEdit(self)
        self.line_name.move(40, 30)
        self.lbl_description = QLabel("Описание:", self)
        self.lbl_description.move(40, 60)
        self.description = QTextEdit(self)
        self.description.setFixedSize(300, 150)
        self.description.move(40, 80)
        self.lbl_src = QLabel("Путь:", self)
        self.lbl_src.move(40, 250)
        self.line_src = QLineEdit(self)
        self.line_src.setFixedWidth(300)
        self.line_src.setReadOnly(True)
        self.line_src.move(40, 270)
        self.btn_src = QPushButton("Обзор",self)
        self.btn_src.move(40, 300)
        self.btn_ok = QPushButton("Ок", self)
        self.btn_ok.move(100, 350)
        self.btn_cancel = QPushButton("Отмена", self)
        self.btn_cancel.move(200, 350)

    def binds(self):
        self.btn_src.clicked.connect(self.src)
        self.btn_cancel.clicked.connect(self.cancel)
        self.btn_ok.clicked.connect(self.ok)

    def cancel(self):
        self.close()

    def src(self):
        fname = QFileDialog.getOpenFileName(self, 'Open file', '/home', '*.png *.jpg')[0]
        self.line_src.setText(fname)

    def ok(self):
        name = self.line_name.text()
        description = self.description.toPlainText()
        src = self.line_src.text()
        if name == "" or src == "":
            MessageBox("Ошибка", "Заполните все поля!")
        else:
            query = QSqlQuery()
            tmp = Progress().listOfProgress
            tmp = [i[0] for i in tmp]
            print(tmp)
            tmp.append(0)
            result = max(tmp) + 1
            img = "\images\ "
            img = img[:-1] + "achievements\ "[:-1] + "ach" + str(result) + '.' + src.split('.')[-1:][0]
            ROOT_DIR = path.dirname(path.abspath(__file__)) + img
            copyfile(src, ROOT_DIR)

            query.prepare("INSERT INTO achievements (achievement, src, description) VALUES (?, ?, ?)")
            query.addBindValue(name)
            query.addBindValue(img)
            query.addBindValue(description)
            query.exec_()
            query.prepare("SELECT id FROM achievements WHERE achievement = ? ")
            query.addBindValue(name)
            query.exec_()
            query.next()
            result = query.value(0)

            sql = "ALTER TABLE progress ADD COLUMN ach" + str(result) + " INTEGER DEFAULT 0"
            query.exec(sql)
            MessageBox("Успех", "Достижение успешно создано!")
            self.close()


class DelProgress(QDialog):
    def __init__(self):
        super().__init__()
        self.count_prog = 0
        self.initUI()
        self.showProgress()
        self.binds()

    def initUI(self):
        self.setWindowTitle("Удалить достижение")
        self.setGeometry(800, 450, 350, 150)
        self.setMaximumSize(350, 150)
        self.setMinimumSize(350, 150)
        self.lbl_name = QLabel("Название достижения:", self)
        self.lbl_name.move(40, 10)
        self.box = QComboBox(self)
        self.box.move(40, 30)
        self.box.setFixedWidth(250)
        self.btn_del = QPushButton("Удалить",self)
        self.btn_del.move(70, 110)
        self.btn_close = QPushButton("Закрыть", self)
        self.btn_close.move(170, 110)

    def showProgress(self):
        self.count_prog = 0
        query = QSqlQuery()
        query.exec_("SELECT achievement FROM achievements")
        while query.next():
            self.box.addItem(query.value(0))
            self.count_prog += 1

    def binds(self):
        self.btn_del.clicked.connect(self.delProgress)
        self.btn_close.clicked.connect(self.close)

    def delProgress(self):
        if self.count_prog != 0:
            item = self.box.currentText()
            query = QSqlQuery()
            query.prepare("SELECT src FROM achievements WHERE achievement = ?")
            query.addBindValue(item)
            query.exec_()
            query.next()
            src = query.value(0)
            remove(src)
            query.prepare("DELETE FROM achievements WHERE achievement = ?;")
            query.addBindValue(item)
            query.exec_()
            self.delColumn()
            self.box.clear()
            self.showProgress()
            MessageBox("Успех", "Достижение успешно удалено")

        else:
            MessageBox("Ошибка,", "Достижения отсутствуют!")

    def delColumn(self):
        query = QSqlQuery()
        query.exec("PRAGMA foreign_keys = off")
        query.exec("BEGIN TRANSACTION")
        query.exec("CREATE TABLE IF NOT EXISTS progress_temp (id INTEGER, groups TEXT, name TEXT)")
        query.prepare("SELECT id FROM achievements WHERE achievement = ?")
        query.addBindValue(self.box.currentText())
        query.exec()
        query.next()
        item = query.value(0)
        print(item)
        list = []
        query.exec("SELECT id FROM achievements")

        while query.next():
            if query.value(0) != item:
                list.append(query.value(0))
        list.reverse()
        print(list)
        sql = ""
        for i in list:
            sql = "ALTER TABLE progress_temp ADD COLUMN ach" + str(i) + " INTEGER DEFAULT 0"
            query.exec(sql)

        query.exec("INSERT INTO progress_temp(id, groups, name) SELECT id, groups, name FROM progress")
        sql = ""
        for i in list:
            sql = "UPDATE progress_temp SET ach" + str(i) + " = (SELECT progress.ach" + str(i) + \
                  " FROM progress WHERE progress.id = progress_temp.id)"
            query.exec(sql)
        query.exec("DROP TABLE progress")
        query.exec("ALTER TABLE progress_temp RENAME TO progress")
        query.exec("COMMIT")
        query.exec("PRAGMA foreign_keys = on")


